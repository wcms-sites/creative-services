core = 7.x
api = 2
; uw_creative_services
projects[uw_creative_services][type] = "module"
projects[uw_creative_services][download][type] = "git"
projects[uw_creative_services][download][url] = "https://git.uwaterloo.ca/MSI/uw_creative_services.git"
projects[uw_creative_services][download][tag] = "7.x-1.4"
projects[uw_creative_services][subdir] = ""
